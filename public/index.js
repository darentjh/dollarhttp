/*
CLIENT SIDE APPLICATION
 */

(function() {

    var CustomerApp = angular.module("CustomerApp", ["ui-router"]);

    var ListCtrl = function() {
        
        var vm = this;
        vm.customers = [];
        
        $http.get("/customers")
            .then(function(result) {
                vm.customers = result.data;
            }).catch(function(err) {
            console.error(">> error: %s", err)
        });
    };
    
    var CustomerConfig = function($stateprovider, $urlRouterProvider) {

        $stateprovider
            .state("list", {
                url: "/list",
                templateUrl: "/state/list.html", //this is angular view/state, NOT HANDLEBARS, will come from public dir
                controller: ["$http", ListCtrl],
                controllerAs: "listCtrl"

            }).state("details", {})
        
        $urlRouterProvider.otherwise("/list"); //this is like the home state, if 
    };

    var CustomerCtrl = function() {
        
    };
    
    CustomerApp.config(["$stateprovider", "$urlRouterProvider", CustomerConfig]);
    CustomerApp.controller("CustomerCtrl", [CustomerCtrl])
})();
