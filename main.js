// the server
var express = require("express");
var app = express();

var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({layout:false}));


//connect to database
var mysql = require("mysql");
//telling JS that this value we are not going to change, so we dont use var, we use const
const pool = mysql.createPool({
    host:"localhost",
    user: "",
    password: "",
    database: "",
    connectionLimit: 3
});

var error = function(code, req, res) {
    res.status(code);
    res.type("text/plain");
    res.end(msg);
};

//Set up Routes; match the url according to the / below
app.get("/customers", function(req, res) {
    pool.getConnection(function(err, conn) {
        if (err) {
            error(400, err, res);
            return;
        }
        try {
            conn.query("select CUSTOMER_ID,NAME,EMAIL from CUSTOMER",
                function(err, rows) {
                    if (err) {
                        error(400, err, res);
                        return;
                    }
                    // res.json will replace the 3 lines below
                    // res.status(200);
                    // res.type("application/json");
                    // res.send(rows);
                    console.info(">> request type: %s", req.headers["Content-Type"]);
                    res.format({
                        "text/html": function() {
                            res.render("customers", {customers:rows});
                        },
                        "application/json": function() {
                            res.json(rows);
                        }, "*/*": function() {
                            res.status(200);
                            res.type("text/plain");
                            res.send(JSON.stringify(rows));
                        }
                    })
                });
        } catch (ex) {
            error(400, ex, res);
        } finally {
            conn.release();
        }
    })
});

app.get("/customer/:cust_id", function(req, res){
    pool.getConnection(function(err, conn){
        try {
            conn.query("select * from CUSTOMER where CUSTOMER_ID = ?",
                [req.params.cust_id],
                function (err, rows) {
                    if (err) {
                        error(400, err);
                        return;
                    }
                    // rows is an array, if rows ha
                    if (rows.length)
                        res.json(rows[0]);
                    else
                        error(404, "Not found: " + req.params.cust_id, res);
                });
        } catch (ex) {}
        finally {
            conn.release();
        }
    });
});

//If prefix is /bower_components, look for it here; this is here above static public because this is more specific
// .use means to ignore .post or .get and just look at the path given
app.use("/bower_components", express.static(__dirname + "/bower_components"));

//Look for giles in public regardless of their prefix
app.use(express.static(__dirname + "/public"));

//Send 404 since file not available; must have 3 parameters req res next in function
//Something like our catch all for this example
app.use(function(req, res, next){
    res.status(404);
    res.type("text/plain");
    res.send("File not found: " + req.originalUrl);
});

app.listen(3000, function(){
    console.info("Application started on port 3000");
});